import logging
import time
from unittest import mock


SMALL_WAIT = 0.1


def test_subscription_integration(japc, japc_mock):
    param_name = "TEST/TestProperty"
    param = japc_mock.mockParameter(param_name)
    japc_mock.whenGetValueThen(
        param, japc_mock.sel("LHC.USER.TEST"), japc_mock.acqVal(param_name, 42, 0)
    )

    callback = mock.Mock()
    exception = mock.Mock()
    sub1 = japc.subscribeParam(
        parameterName=param_name,
        timingSelectorOverride="LHC.USER.TEST",
        onValueReceived=callback,
        onException=exception,
        getHeader=True,
    )
    # Check that no update is sent before the monitoring starts.
    time.sleep(SMALL_WAIT)
    callback.assert_not_called()
    sub1.startMonitoring()

    # Check that the initial value is sent.
    time.sleep(SMALL_WAIT)
    assert callback.call_args[0][:2] == (param_name, 42)
    assert callback.call_args[0][2]['isFirstUpdate'] is True
    callback.reset_mock()

    # Check that a new value is sent.
    param.setValue(japc_mock.sel("LHC.USER.TEST"), japc_mock.spv(112))
    time.sleep(SMALL_WAIT)
    assert callback.call_args[0][:2] == (param_name, 112)
    assert callback.call_args[0][2]['isFirstUpdate'] is False
    callback.reset_mock()

    # Now test an exception being raised.
    japc_mock.dispatch(param, japc_mock.sel("LHC.USER.TEST"), japc_mock.pe("Some exception"))
    time.sleep(SMALL_WAIT)
    assert exception.call_args[0][:2] == (param_name, "Some exception")


def test_default_exception_handler(japc, japc_mock, caplog):
    param_name = "TEST/TestProperty"
    param = japc_mock.mockParameter(param_name)
    japc_mock.whenGetValueThen(
        param, japc_mock.sel("LHC.USER.TEST"), japc_mock.acqVal(param_name, 42, 0)
    )

    sub1 = japc.subscribeParam(
        parameterName=param_name,
        timingSelectorOverride="LHC.USER.TEST",
        onValueReceived=mock.Mock(),
    )
    sub1.startMonitoring()
    japc_mock.dispatch(param, japc_mock.sel("LHC.USER.TEST"), japc_mock.pe("Some exception"))
    time.sleep(SMALL_WAIT*10)

    # The default onException handler writes to the japc.log.
    assert (
        "pyjapc",
        logging.ERROR,
        "Parameter TEST/TestProperty received exception:\nSome exception",
    ) in caplog.record_tuples
