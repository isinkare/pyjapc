# For examples of using JAPC's mockito implementation, see:
# https://gitlab.cern.ch/acc-co/japc/japc-core/blob/develop/japc-ext-mockito2/src/java/cern/japc/ext/mockito/demo/Demo.java

import gc
import datetime
import time
from unittest import mock as unittest_mock

import jpype as jp
import numpy as np
import numpy.testing
import pytest

from pyjapc import PyJapc
from pyjapc._japc import _INSTANCE_DEFAULT  # type: ignore


@pytest.fixture
def simple_descriptor(jvm):
    return jp.JClass("cern.japc.core.spi.value.SimpleDescriptorImpl")


@pytest.mark.parametrize('dtype', [
    np.int16,
    np.int32,
    np.int64,
    np.float32,
    np.float64,
    np.double,
    bool,
    int,
    float,
    str,
])
def test_conversion_roundtrip(japc, dtype):
    """ Take a random value of type dtype,
        convert into Java World and back.
        Check if value changed
    """
    x = dtype(np.random.random() * 4200)
    y = japc._convertValToPy(japc._convertPyToVal(x))
    assert (x == y)


def test_convert_py_to_val_int(japc, simple_descriptor):
    japc_type = jp.JClass("cern.japc.value.spi.value.simple.IntValue")
    value_descriptor = simple_descriptor(jp.JClass("cern.japc.value.ValueType").INT)

    val = 3
    r = japc._convertPyToVal(val, value_descriptor)
    assert r != japc_type(val + 1)
    assert r == japc_type(val)

    back = japc._convertValToPy(r)
    assert back == val


def round_trip_double(japc, value):
    simple_descriptor = jp.JClass("cern.japc.core.spi.value.SimpleDescriptorImpl")
    value_descriptor = simple_descriptor(jp.JClass("cern.japc.value.ValueType").DOUBLE)
    java_val = japc._convertPyToVal(value, value_descriptor)
    round_tripped_value = japc._convertValToPy(java_val)
    return java_val, round_tripped_value


@pytest.mark.parametrize(['value'], [[3.14], [-np.inf], [np.inf]])
def test_convert_py_to_val_float(japc, value):
    japc_type = jp.JClass("cern.japc.value.spi.value.simple.DoubleValue")
    java_val, py_val = round_trip_double(japc, value)
    assert java_val == japc_type(value)
    assert py_val == value


def test_convert_py_to_val_nan(japc):
    japc_type = jp.JClass("cern.japc.value.spi.value.simple.DoubleValue")
    java_val, py_val = round_trip_double(japc, np.nan)
    assert java_val == japc_type(np.nan)
    assert np.isnan(py_val)


@pytest.mark.parametrize(['jprimitive', 'expectation'], [
    ["jp.JInt(42)", "cern.japc.value.spi.value.simple.IntValue"],
    ["jp.JFloat(3.14)", "cern.japc.value.spi.value.simple.FloatValue"],
    ["jp.JDouble(3.14)", "cern.japc.value.spi.value.simple.DoubleValue"],
    ["jp.JLong(42)", "cern.japc.value.spi.value.simple.LongValue"],
    ["jp.JBoolean(False)", "cern.japc.value.spi.value.simple.BooleanValue"],
    ["jp.JByte(ord(b'm'))", "cern.japc.value.spi.value.simple.ByteValue"],
    ["jp.JShort(42)", "cern.japc.value.spi.value.simple.ShortValue"],
    ["jp.JString('abc')", "cern.japc.value.spi.value.simple.StringValue"],
])
def test_convert_py_to_val_primitive(japc, jprimitive, expectation):
    # Check that PyToVal can handle primitive Java types.
    jprimitive = eval(jprimitive)
    if isinstance(expectation, str):
        # A simple string was given as the expectation - this represents the
        # expected class type.
        result = japc._convertPyToVal(jprimitive)
        expected_type = jp.JClass(expectation)
        assert isinstance(result, expected_type)


def test_convert_py_to_val_w_simple_descriptor(japc, simple_descriptor):
    value_descriptor = simple_descriptor(
        jp.JClass("cern.japc.value.ValueType").DOUBLE)

    r = japc._convertPyToVal(4.1, vdesc=value_descriptor, checkDims=False)
    assert r == jp.JClass("cern.japc.value.spi.value.simple.DoubleValue")(4.1)


def test_convert_py_to_val_w_map_descriptor(japc, japc_mock, simple_descriptor):
    double_descriptor = simple_descriptor(
        jp.JClass("cern.japc.value.ValueType").DOUBLE)
    int_descriptor = simple_descriptor(
        jp.JClass("cern.japc.value.ValueType").INT)

    map_descriptor = jp.JClass("cern.japc.core.spi.value.MapDescriptorImpl")(
        ['a_double', 'an_int', 'another_double'],
        [double_descriptor, int_descriptor, double_descriptor],
    )

    with pytest.raises(AttributeError, match='object has no attribute \'items\''):
        # The current behaviour is to raise obscurely if a non-dict is passed.
        japc._convertPyToVal(4.1, vdesc=map_descriptor, checkDims=False)

    with pytest.raises(NameError, match="Field s does not exist in Parameter None"):
        # The current behaviour for poorly defined dicts is to raise a NameError.
        japc._convertPyToVal({'s': 1}, vdesc=map_descriptor, checkDims=False)

    result = japc._convertPyToVal(
        {'a_double': 1, 'another_double': 3.14, 'an_int': -2},
        vdesc=map_descriptor, checkDims=False,
    )

    expected = jp.JClass("cern.japc.core.spi.value.MapParameterValueImpl")(
        ['a_double', 'an_int', 'another_double'], [
            # Note that the "a_double" is cast to an IntValue.
            jp.JClass("cern.japc.value.spi.value.simple.IntValue")(1),
            jp.JClass("cern.japc.value.spi.value.simple.IntValue")(-2),
            jp.JClass("cern.japc.value.spi.value.simple.DoubleValue")(3.14),
        ]
    )
    assert result == expected


@pytest.mark.parametrize(['jprimitive'], [
    ['jp.JChar(42)'],
])
def test_convert_py_to_val_primitive_illegal_argument(japc, jprimitive):
    jprimitive = eval(jprimitive)  # Must be run after JVM is started.
    with pytest.raises(TypeError,
                       match="Ambiguous overloads found for"):
        japc._convertPyToVal(jprimitive)


@pytest.mark.parametrize(
    ['enum_code', 'enum_value', 'enum_str'],
    [(42, "ON", "(Enum:1) -> ON"),
     (-5, "OFF", "(Enum:1) -> OFF"),
])
def test_convert_py_to_val_roundtrip_enum(
        japc, std_meaning_enum_descriptor, enum_code, enum_value, enum_str):

    res = japc._convertPyToVal(enum_code, std_meaning_enum_descriptor)
    assert str(res) == enum_str
    assert japc._convertValToPy(res) == (enum_code, enum_value)

    res = japc._convertPyToVal(enum_value, std_meaning_enum_descriptor)
    assert str(res) == enum_str
    assert japc._convertValToPy(res) == (enum_code, enum_value)


@pytest.mark.parametrize(
    ['to_convert'],
    [[1],
     ['NOT_VALID'],
])
def test_invalid_enum(japc, std_meaning_enum_descriptor, to_convert):
    with pytest.raises(jp.java.lang.RuntimeException):
        japc._convertPyToVal(to_convert, std_meaning_enum_descriptor)


@pytest.fixture
def enumset_vdesc(enumtype_byte, simple_descriptor):
    value_descriptor = simple_descriptor(
        jp.JClass("cern.japc.value.ValueType").ENUM_SET, enumtype_byte)
    yield value_descriptor


@pytest.mark.parametrize(
    ['py_value', 'enumset_str'],
    [(0, "(EnumSet:1) -> []"),
     (2 | 4, "(EnumSet:1) -> [Item 2, Item 3]"),
])
def test_convert_py_to_val_enumset(japc, enumset_vdesc, py_value, enumset_str):

    res = japc._convertPyToVal(py_value, enumset_vdesc)
    assert isinstance(res, jp.JClass('cern.japc.value.spi.value.simple.EnumSetValue'))
    assert str(res) == enumset_str


@pytest.mark.parametrize(
    ['py_value'],
    [("[Item 2, Item 3]",),
     ([],),
     ([1, 2],),
     ([(1, "Item 1")],),
])
def test_convert_py_to_val_enumset_invalid_types(japc, enumset_vdesc, py_value):
    # The error should come straight out of JPype.
    expected_exception = pytest.raises(
        TypeError,
        match=("No matching overloads found for "
               "cern.japc.value.spi.value.simple.EnumSetValue"))

    with expected_exception:
        japc._convertPyToVal(py_value, enumset_vdesc)


@pytest.mark.parametrize(
    ['enumset_code', 'expected_py'],
    [(0, []),
     (1, [(1, 'Item 1')]),
     (2, [(2, 'Item 2')]),
     (1 | 2, [(1, 'Item 1'), (2, 'Item 2')]),
     (1 | 2 | 3, [(1, 'Item 1'), (2, 'Item 2')]),
     (2 | 4 | 8, [(2, 'Item 2'), (4, 'Item 3'), (8, 'Item 4')]),
     (1 | 4 | 8 | 16, [(1, 'Item 1'), (4, 'Item 3'), (8, 'Item 4')]),
])
def test_convert_val_to_py_enumset(japc, enumtype_byte, enumset_code, expected_py):
    SimpleParameterValueFactory = jp.JClass(
        "cern.japc.core.factory.SimpleParameterValueFactory")

    EnumItemSetImpl = jp.JClass("cern.japc.value.spi.value.EnumItemSetImpl")
    enum_set_value = SimpleParameterValueFactory.newSimpleParameterValue(
        EnumItemSetImpl(enumtype_byte, enumset_code)
    )

    res = japc._convertValToPy(enum_set_value)
    assert res == expected_py


def test_convert_py_to_simple(japc):
    assert japc._convertPyToSimpleVal(1).toString() == '(int:1) -> 1'
    assert japc._convertPyToSimpleVal(1.1).toString() == '(double:1) -> 1.1'
    assert japc._convertPyToSimpleVal(True).toString() == '(boolean:1) -> true'
    assert japc._convertPyToSimpleVal(False).toString() == '(boolean:1) -> false'
    assert japc._convertPyToSimpleVal("HelloTesti").toString() == '(String:1) -> HelloTesti'

    a = np.array(["hello", "world", "TestiUltraLongBlaString"])
    ja = japc._convertPyToSimpleVal(a)
    res = japc._convertSimpleValToPy(ja)
    assert res[0] == "hello"
    assert res[1] == "world"
    assert res[2] == "TestiUltraLongBlaString"
    assert res.size == 3
    sVal = japc._convertPyToVal(
        {"a": 4, "e": False, "b": np.ones((2, 2)), "c": "Wow", "d": True})
    assert sVal.toString() == (
        'a (int:1) -> 4\n'
        'b (double[][]:2x2) -> [1.0, 1.0], [1.0, 1.0]\n'
        'c (String:1) -> Wow\nd (boolean:1) -> true\n'
        'e (boolean:1) -> false\n')
    a = np.array([1, 1, 0, 1], dtype=bool)
    assert japc._convertPyToSimpleVal(a).toString() == '(boolean[]:4) -> true, true, false, true'


@pytest.mark.parametrize(['np_array', 'arr_val_type'], [
    [np.arange(4, dtype=np.double).reshape(2, 2),
     "cern.japc.value.spi.value.simple.DoubleArrayValue"],
    [np.array(['one', 'three', 'five', 'seven'], dtype=np.str_).reshape(2, 2),
     "cern.japc.value.spi.value.simple.StringArrayValue"],
    [np.arange(0, dtype=np.double),
     "cern.japc.value.spi.value.simple.DoubleArrayValue"],
    pytest.param(
        np.array([[]], dtype=np.int16).reshape(0, 0),
        "cern.japc.value.spi.value.simple.ShortArrayValue",
        marks=pytest.mark.xfail,
    ),
    pytest.param(
        np.array([[]], dtype=np.byte).reshape(0, 0),
        "cern.japc.value.spi.value.simple.ByteArrayValue",
        marks=pytest.mark.xfail,
    ),
    pytest.param(
        np.array([[1], [2]], dtype=np.int16),
        "cern.japc.value.spi.value.simple.ShortArrayValue",
        marks=pytest.mark.xfail,
    ),
    pytest.param(
        np.array([[1], [2]], dtype=np.byte),
        "cern.japc.value.spi.value.simple.ByteArrayValue",
        marks=pytest.mark.xfail,
    )
])
def test_array_conversions(japc, np_array, arr_val_type):
    j_array = japc._convertPyToVal(np_array)
    arr_type = jp.JClass(arr_val_type)
    assert isinstance(j_array, arr_type)
    j2np_array = japc._convertValToPy(j_array)
    assert np.array_equal(np_array, j2np_array)
    assert isinstance(j2np_array, np.ndarray)


def test_double_array_to_py_conversion(japc):
    arr = np.array([1.2, 2.4, 4.8], dtype=np.float64)
    DoubleArrayValue = jp.JClass("cern.japc.value.spi.value.simple.DoubleArrayValue")
    v = DoubleArrayValue(jp.JArray(jp.JDouble)(arr))
    r = japc._convertSimpleValToPy(v)
    assert isinstance(r, np.ndarray)
    np.testing.assert_array_equal(r, arr)
    assert r.dtype == np.float64


def test_float_array_to_py_conversion(japc):
    arr = np.array([1.2, 2.4, 4.8], dtype=np.float32)
    FloatArrayValue = jp.JClass("cern.japc.value.spi.value.simple.FloatArrayValue")
    v = FloatArrayValue(jp.JArray(jp.JFloat)(arr))
    r = japc._convertSimpleValToPy(v)
    assert isinstance(r, np.ndarray)
    np.testing.assert_array_equal(r, arr)
    assert r.dtype == np.float32


def test_py_to_simple_discrete_function(japc, simple_descriptor):
    np_array = np.arange(4, dtype=np.double).reshape(2, 2)
    j_array = japc._convertPyToVal(np_array, dtype="DiscreteFunction")
    arr_type = jp.JClass("cern.japc.value.spi.value.simple.DiscreteFunctionValue")
    assert isinstance(j_array, arr_type)

    py_arr = japc._convertSimpleValToPy(j_array)
    np.testing.assert_array_equal(py_arr, np_array)

    value_descriptor = simple_descriptor(jp.JClass("cern.japc.value.ValueType").DISCRETE_FUNCTION)
    j_array = japc._convertPyToVal(np_array, dtype="DiscreteFunction", vdesc=value_descriptor)
    assert isinstance(j_array, arr_type)

    with pytest.raises(TypeError, match="Please provide a 2D array of shape"):
        japc._convertPyToVal(np_array.reshape(2, 1, 2), dtype="DiscreteFunction", vdesc=value_descriptor)


def test_py_to_simple_discrete_function_list(japc, simple_descriptor):
    list_of_2d_functions = [np.arange(4, dtype=np.double).reshape(2, 2), ]
    j_array = japc._convertPyToVal(list_of_2d_functions, dtype="DiscreteFunctionList")
    arr_type = jp.JClass("cern.japc.value.spi.value.simple.DiscreteFunctionListValue")
    assert isinstance(j_array, arr_type)

    py_arr = japc._convertSimpleValToPy(j_array)
    assert isinstance(py_arr, list)
    np.testing.assert_array_equal(py_arr[0], list_of_2d_functions[0])

    value_descriptor = simple_descriptor(jp.JClass("cern.japc.value.ValueType").DISCRETE_FUNCTION_LIST)
    j_array = japc._convertPyToVal(list_of_2d_functions, dtype="DiscreteFunctionList", vdesc=value_descriptor)
    assert isinstance(j_array, arr_type)

    with pytest.raises(TypeError, match="Please provide a list of 2D arrays of shape"):
        japc._convertPyToVal(
            [np_array.reshape(2, 1, 2) for np_array in list_of_2d_functions],
             dtype="DiscreteFunction", vdesc=value_descriptor
        )


def test_parameter_groups(japc, japc_mock):
    # NOTE: Requires all services to be mocked, hence the need for japc_mock.
    p1 = "LHC.BQTrig.HB1/Acquisition#sequence"
    p2 = "LHC.BQTrig.HB2/Acquisition#sequence"
    gr = japc._getJapcPar([p1, p2])
    assert isinstance(gr, jp.JClass("cern.japc.core.spi.group.ParameterGroupImpl"))
    gr = japc._getJapcPar([p1])
    assert isinstance(gr, jp.JClass("cern.japc.core.spi.group.ParameterGroupImpl"))
    pa = japc._getJapcPar(p1)
    assert isinstance(pa, jp.JClass("cern.japc.core.spi.adaptation.FieldFilteringParameterAdapter"))


def test_selector_business(jvm):
    japc_obj = PyJapc(incaAcceleratorName=None)
    assert japc_obj.getSelector() == "LHC.USER.ALL"
    assert japc_obj.getDataFilter() is None
    japc_obj.setSelector("ALL", )
    assert japc_obj.getSelector() == "ALL"
    assert japc_obj.getDataFilter() is None
    japc_obj.setSelector("LHC.USER.ALL", {"averaging": 1})
    assert japc_obj.getSelector() == "LHC.USER.ALL"
    assert japc_obj._selector.getDataFilter().toString() == "averaging (int:1) -> 1\n"


def test_setDataFilter(jvm):
    japc_obj = PyJapc(incaAcceleratorName=None)
    assert japc_obj.getDataFilter() is None
    japc_obj.setDataFilter({'averaging': 1})
    assert japc_obj.getDataFilter() == {'averaging': 1}
    japc_obj.setDataFilter(None)
    assert japc_obj.getDataFilter() is None


def test_get_value(japc, japc_mock):
    param = 'TEST/TestProperty'
    mock = japc_mock.mockParameter(param)
    japc_mock.whenGetValueThen(
        mock, japc_mock.sel('LHC.USER.TEST'), japc_mock.acqVal(param, 42, 0))
    japc.setSelector('LHC.USER.TEST')
    assert japc.getParam('TEST/TestProperty') == 42


def test_get_value_exception(japc, japc_mock):
    param = 'TEST/TestProperty'
    mock = japc_mock.mockParameter(param)
    japc_mock.whenGetValueThen(
        mock, japc_mock.sel('LHC.USER.TEST'),
        japc_mock.pe("MyException"),
    )
    japc.setSelector('LHC.USER.TEST')
    with pytest.raises(jp.JPackage('cern').japc.core.ParameterException, match="MyException"):
        japc.getParam('TEST/TestProperty')


def test_get_value_header(japc, japc_mock):
    param = 'TEST/TestProperty'
    mock = japc_mock.mockParameter(param)

    japc_mock.whenGetValueThen(mock, japc_mock.sel('SPS.USER.SFTPRO'), 42)
    japc.setSelector('SPS.USER.SFTPRO')
    values, headers = japc.getParam(param, getHeader=True)
    assert values == 42
    assert isinstance(headers, dict)
    assert isinstance(headers.pop('acqStamp'), datetime.datetime)
    assert isinstance(headers.pop('cycleStamp'), datetime.datetime)
    assert isinstance(headers.pop('setStamp'), datetime.datetime)
    assert headers == dict(
        isFirstUpdate=False,
        isImmediateUpdate=False,
        # TODO: The selector to be corrected after https://gitlab.cern.ch/acc-co/japc/japc-core/-/merge_requests/9.
        selector='__unknown_selector__',
    )


def test_async_get(japc, japc_mock):
    param = 'TEST/TestProperty'
    callback = unittest_mock.Mock()
    mock = japc_mock.mockParameter(param)
    japc_mock.whenGetValueThen(mock, japc_mock.sel('LHC.USER.TEST'), japc_mock.acqVal(param, 42, 0))
    japc.setSelector('LHC.USER.TEST')
    japc.getParam(parameterName='TEST/TestProperty', onValueReceived=callback)
    time.sleep(1)
    callback.assert_called_with(42)


def test_set_value(japc, japc_mock):
    param = 'TEST/TestProperty'
    mock = japc_mock.mockParameter(param)
    japc.setSelector('LHC.USER.TEST')
    japc.setParam(param, 1.0, checkDims=False)
    jp.JClass("org.mockito.Mockito").verify(mock).setValue(
        japc_mock.sel('LHC.USER.TEST'), japc_mock.spv(1.0))


@pytest.mark.xfail
def test_get_all_selector(japc, japc_mock):
    # Follows the pattern introduced by https://gitlab.cern.ch/acc-co/japc/japc-core/-/merge_requests/9
    param = 'TEST/TestProperty'
    mock = japc_mock.mockParameter(param)

    japc_mock.whenGetValueThen(mock, japc_mock.sel('SPS.USER.SFTPRO'), 41)
    japc_mock.whenGetValueThen(mock, japc_mock.sel('SPS.USER.CNGS'), 42)

    Cycle = jp.JPackage('cern').japc.ext.mockito.Cycle
    spsSuperCycle = japc_mock.newSuperCycle(
        Cycle("SPS.USER.SFTPRO", 3000),
        Cycle("SPS.USER.CNGS", 2000),
    )
    spsSuperCycle.start()

    japc.setSelector('SPS.USER.ALL')
    values, headers = japc.getParam(param, getHeader=True)

    assert values == 42
    assert isinstance(headers, dict)
    assert isinstance(headers.pop('acqStamp'), datetime.datetime)
    assert isinstance(headers.pop('cycleStamp'), datetime.datetime)
    assert isinstance(headers.pop('setStamp'), datetime.datetime)
    assert headers == dict(
        isFirstUpdate=False,
        isImmediateUpdate=False,
        selector='SPS.USER.SFTPRO',
    )
    spsSuperCycle.stop()


def test_array_of_strings(japc):
    arr = np.array(['one', 'three', 'five', 'seven'], dtype=np.str_)
    StringValueArray = jp.JClass("cern.japc.value.spi.value.simple.StringArrayValue")
    v = StringValueArray(jp.JArray(jp.JString)(arr))
    r = japc._convertSimpleValToPy(v)
    assert isinstance(r, np.ndarray)
    np.testing.assert_array_equal(r, arr)

    v = StringValueArray(jp.JArray(jp.JString)(arr[:1]))
    r = japc._convertSimpleValToPy(v)
    # Note that the implementation unpacks single string arrays into a single string.
    # To be fixed by https://issues.cern.ch/browse/ACCPY-724.
    assert isinstance(r, str)
    assert r == "one"
    np.testing.assert_array_equal(r, arr[:1])

    # When converting an empty list of string we end up with an empty array which
    # is of float type! This should be addressed in PyJapc v3.
    v = StringValueArray(jp.JArray(jp.JString)(arr[:0]))
    r = japc._convertSimpleValToPy(v)
    assert isinstance(r, np.ndarray)
    assert r.dtype == np.float64
    assert r.size == 0


def test_acquired_parameter_value_strings(japc):
    selector = jp.JClass("cern.japc.core.factory.SelectorFactory").newSelector("")
    value_header = jp.JClass("cern.japc.core.spi.ValueHeaderImpl")(123, selector)

    param_value = jp.JClass("cern.japc.value.spi.value.SimpleParameterValueImpl")()

    AcquiredParameterValueImpl = jp.JClass("cern.japc.core.spi.AcquiredParameterValueImpl")
    apv = AcquiredParameterValueImpl("my_param_name", value_header, param_value)

    param_value.setString("foobar")
    r = japc._processTempValue(apv, getHeader=False, noPyConversion=False)
    assert r == "foobar"

    param_value.setString("")
    r = japc._processTempValue(apv, getHeader=False, noPyConversion=False)
    assert r == ""

    # Some weird behaviour in JAPC means that if we set something other than a string, e.g. a bool,
    # then the result is a string, not a bool object.
    param_value.setBoolean(False)
    r = japc._processTempValue(apv, getHeader=False, noPyConversion=False)
    assert r == "false"

    param_value = jp.JClass("cern.japc.value.spi.value.SimpleParameterValueImpl")()
    param_value.setBoolean(False)
    apv = AcquiredParameterValueImpl("my_param_name", value_header, param_value)
    r = japc._processTempValue(apv, getHeader=False, noPyConversion=False)
    assert r is False


def test_multiple_selectors_same_no_param(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert 'TEST/TestProperty1@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty1@LHC.USER.TEST2' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()
    japc.stopSubscriptions()
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()


def test_multiple_selectors_same_no_selector_on_subscribe(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert 'TEST/TestProperty1@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty1@LHC.USER.TEST2' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert not japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST1'][0].isMonitoring()
    assert not japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST2'][0].isMonitoring()
    assert not japc._subscriptionHandleDict['TEST/TestProperty2@LHC.USER.TEST1'][0].isMonitoring()
    japc.startSubscriptions('TEST/TestProperty1')
    assert japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST1'][0].isMonitoring()
    assert japc._subscriptionHandleDict['TEST/TestProperty1@LHC.USER.TEST2'][0].isMonitoring()
    assert not japc._subscriptionHandleDict['TEST/TestProperty2@LHC.USER.TEST1'][0].isMonitoring()


def test_multiple_selectors_same_no_selector_on_unsubscribe(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()
    japc.stopSubscriptions('TEST/TestProperty1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert sub3.isMonitoring()


def test_multiple_selectors_same_with_selector_on_subscribe(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions(parameterName='TEST/TestProperty1', selector='LHC.USER.TEST1')
    assert sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()


def test_multiple_selectors_same_with_selector_on_unsubscribe(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    sub1 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST1')
    sub2 = japc.subscribeParam(parameterName='TEST/TestProperty1', timingSelectorOverride='LHC.USER.TEST2')
    sub3 = japc.subscribeParam(parameterName='TEST/TestProperty2', timingSelectorOverride='LHC.USER.TEST1')
    assert 'TEST/TestProperty1@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty1@LHC.USER.TEST2' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2@LHC.USER.TEST1' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    assert not sub3.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()
    japc.stopSubscriptions(parameterName='TEST/TestProperty1', selector='LHC.USER.TEST1')
    assert not sub1.isMonitoring()
    assert sub2.isMonitoring()
    assert sub3.isMonitoring()


def test_subscribe_specific_no_selectors(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    japc.startSubscriptions('TEST/TestProperty1')
    assert sub1.isMonitoring()
    assert not sub2.isMonitoring()


def test_subscribe_all_no_selectors(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    japc.startSubscriptions()
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()


def test_unsubscribe_specific_no_selectors(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    japc.startSubscriptions()
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    japc.stopSubscriptions('TEST/TestProperty1')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert sub2.isMonitoring()


def test_unsubscribe_all_no_selectors(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    param2 = 'TEST/TestProperty2'
    mocj1 = japc_mock.mockParameter(param1)
    mocj2 = japc_mock.mockParameter(param2)
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1')
    sub2 = japc.subscribeParam('TEST/TestProperty2')
    japc.startSubscriptions()
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    japc.stopSubscriptions()
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()


def test_subscribe_multiple_subscriptions_same_param_no_selectors(japc, japc_mock):
    param1 = 'TEST/TestProperty1'
    mocj1 = japc_mock.mockParameter(param1)
    def handler1(p, v):
        pass
    def handler2(p, v):
        pass
    japc.setSelector('LHC.USER.TEST')
    sub1 = japc.subscribeParam('TEST/TestProperty1', handler1)
    sub2 = japc.subscribeParam('TEST/TestProperty1', handler2)
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()
    japc.startSubscriptions('TEST/TestProperty1')
    assert sub1.isMonitoring()
    assert sub2.isMonitoring()
    japc.stopSubscriptions('TEST/TestProperty1')
    assert not sub1.isMonitoring()
    assert not sub2.isMonitoring()


def test_clear_subscriptions_no_args(japc, japc_mock):
    japc_mock.mockParameter('TEST/TestProperty1')
    japc_mock.mockParameter('TEST/TestProperty2')
    japc.setSelector('LHC.USER.TEST')
    japc.subscribeParam('TEST/TestProperty1')
    japc.subscribeParam('TEST/TestProperty2')
    assert 'TEST/TestProperty1' in japc._subscriptionHandleDict
    assert 'TEST/TestProperty2' in japc._subscriptionHandleDict
    assert len(japc._subscriptionHandleDict['TEST/TestProperty1']) == 1
    assert len(japc._subscriptionHandleDict['TEST/TestProperty2']) == 1
    japc.clearSubscriptions()
    assert len(japc._subscriptionHandleDict) == 0


@pytest.mark.parametrize('subscribe_params, clear_param, clear_selector, remaining_subscriptions', [
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', None)], None, None, {}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', None)], None, None, {}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', 'LHC.USER.TEST')], None, None, {}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', None)], 'TEST/TestProperty1', None, {'TEST/TestProperty2': 1}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', None)], 'TEST/TestProperty1', None, {'TEST/TestProperty1@SPS.USER.TEST': 1, 'TEST/TestProperty2': 1}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', 'LHC.USER.TEST')], 'TEST/TestProperty1', None, {'TEST/TestProperty2@LHC.USER.TEST': 1}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', 'LHC.USER.TEST')], 'TEST/TestProperty1', None, {'TEST/TestProperty1@SPS.USER.TEST': 1, 'TEST/TestProperty2@LHC.USER.TEST': 1}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', None)], 'TEST/TestProperty1', 'LHC.USER.TEST', {'TEST/TestProperty1': 1, 'TEST/TestProperty2': 1}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', None)], 'TEST/TestProperty1', 'LHC.USER.TEST', {'TEST/TestProperty1@SPS.USER.TEST': 1, 'TEST/TestProperty2': 1}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', 'LHC.USER.TEST')], 'TEST/TestProperty1', 'LHC.USER.TEST', {'TEST/TestProperty1': 1, 'TEST/TestProperty2@LHC.USER.TEST': 1}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', 'LHC.USER.TEST')], 'TEST/TestProperty1', 'LHC.USER.TEST', {'TEST/TestProperty1@SPS.USER.TEST': 1, 'TEST/TestProperty2@LHC.USER.TEST': 1}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', None)], 'TEST/TestProperty1', 'SPS.USER.TEST', {'TEST/TestProperty1': 1, 'TEST/TestProperty2': 1}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', None)], 'TEST/TestProperty1', 'SPS.USER.TEST', {'TEST/TestProperty2': 1}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', 'LHC.USER.TEST')], 'TEST/TestProperty1', 'SPS.USER.TEST', {'TEST/TestProperty1': 1, 'TEST/TestProperty2@LHC.USER.TEST': 1}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', 'LHC.USER.TEST')], 'TEST/TestProperty1', 'SPS.USER.TEST', {'TEST/TestProperty2@LHC.USER.TEST': 1}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', None)], None, 'LHC.USER.TEST', {}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', None)], None, 'LHC.USER.TEST', {}),
    ([('TEST/TestProperty1', None), ('TEST/TestProperty2', 'LHC.USER.TEST')], None, 'LHC.USER.TEST', {}),
    ([('TEST/TestProperty1', 'SPS.USER.TEST'), ('TEST/TestProperty2', 'LHC.USER.TEST')], None, 'LHC.USER.TEST', {}),
    # TODO: This test should not pass in PyJapc v3 (since None == '' selector).
    ([('TEST/TestProperty1', _INSTANCE_DEFAULT), ], 'TEST/TestProperty1', None, {}),
    # TODO: This test should pass in PyJapc v3 - we have specified a selector which happens to be the default one,
    #  and we should therefore clear the subscription.
    pytest.param(
        [('TEST/TestProperty1', _INSTANCE_DEFAULT), ], 'TEST/TestProperty1', 'LHC.USER.TEST', {},
        marks=pytest.mark.xfail(reason='pyjapc bug which means that the instance selector is not part of the subscription key')),
])
def test_clear_subscriptions_with_args(japc, japc_mock, subscribe_params, clear_param, clear_selector, remaining_subscriptions):
    for param, _ in subscribe_params:
        japc_mock.mockParameter(param)

    for param, selector in subscribe_params:
        if selector is _INSTANCE_DEFAULT:
            # PyJapc v2 distinguishes whether the timingSelectorOverride was set or not.
            # There is no value you can set to prevent this (None has a meaning),
            # so we honour this behaviour.
            # TODO: To be normalized in PyJapc v3.
            japc.subscribeParam(parameterName=param)
        else:
            japc.subscribeParam(parameterName=param, timingSelectorOverride=selector)

    japc.clearSubscriptions(parameterName=clear_param, selector=clear_selector)

    assert set(japc._subscriptionHandleDict) == set(remaining_subscriptions)
    for key, length in remaining_subscriptions.items():
        assert len(japc._subscriptionHandleDict[key]) == length


def test_subscription_callback_object_deletion(japc, japc_mock):
    # A bound-method callback passed to a subscription should not hold on to a
    # reference once it has been cleared.
    destroyed = False

    class ObjectWCallbackMethod:
        def callback(self, *args, **kwargs):
            pass

        def __del__(self):
            nonlocal destroyed
            destroyed = True

    japc.subscribeParam(
        'TEST/TestProperty1', onValueReceived=ObjectWCallbackMethod().callback)
    japc.clearSubscriptions()

    # Mockito holds on to a reference the subscription, so clear that.
    # There is no such global state in the RDA3 implementation
    japc_mock.resetToDefault()

    # Force the Python GC to trigger. Outside of a test case we would normally
    # just let this happen naturally.
    gc.collect()

    # Java GC is not instant, so let's give it a chance to finish.
    for _ in range(10):
        if destroyed:
            break
        time.sleep(0.1)

    assert destroyed


def test_get_users(japc):
    assert japc.getUsers('LHC') == ['LHC.USER.ALL']
    assert japc.getUsers('nothing') == ['nothing.USER.ALL']


def test__getSimpleValFromDesc(japc, simple_descriptor):
    japc_type = jp.JClass("cern.japc.value.spi.value.simple.IntValue")
    value_descriptor = simple_descriptor(jp.JClass("cern.japc.value.ValueType").INT)
    s = japc._getSimpleValFromDesc(value_descriptor)
    assert isinstance(s, japc_type)
