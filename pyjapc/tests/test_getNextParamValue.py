import threading
import time

import pytest


def test_simple_case(japc, japc_mock):
    param_name = 'TEST/TestProperty'
    param = japc_mock.mockParameter(param_name)
    japc_mock.whenGetValueThen(
        param, japc_mock.sel('LHC.USER.TEST'), japc_mock.acqVal(param_name, 42, 0))

    with pytest.raises(TimeoutError):
        japc.getNextParamValue(param_name, timeout=1)

    def set_value():
        param.setValue(japc_mock.sel("LHC.USER.TEST"), japc_mock.spv(112))

    threading.Timer(0.01, set_value).start()
    result = japc.getNextParamValue(param_name, timeout=1, timingSelectorOverride="LHC.USER.TEST")
    assert result == 112

    threading.Timer(0.01, set_value).start()
    result = japc.getNextParamValue(param_name, getHeader=True, timeout=1, timingSelectorOverride="LHC.USER.TEST")
    assert result[0] == 112
    assert 'isFirstUpdate' in result[1]


def test_n_values(japc, japc_mock):
    param_name = 'TEST/TestProperty'
    param = japc_mock.mockParameter(param_name)
    japc_mock.whenGetValueThen(
        param, japc_mock.sel('LHC.USER.TEST'), japc_mock.acqVal(param_name, 42, 0))

    def set_several_values():
        param.setValue(japc_mock.sel("LHC.USER.TEST"), japc_mock.spv(112))
        time.sleep(0.01)
        param.setValue(japc_mock.sel("LHC.USER.TEST"), japc_mock.spv(113))
        time.sleep(0.01)
        param.setValue(japc_mock.sel("LHC.USER.TEST"), japc_mock.spv(-1))

    threading.Timer(0.01, set_several_values).start()
    result = japc.getNextParamValue(param_name, timeout=1, timingSelectorOverride="LHC.USER.TEST", n_values=3)
    assert result == [112, 113, -1]

    time.sleep(0.1)

    threading.Timer(0.01, set_several_values).start()
    result = japc.getNextParamValue(param_name, timeout=1, timingSelectorOverride="LHC.USER.TEST", n_values=3, getHeader=True)
    assert [v[0] for v in result] == [112, 113, -1]
    assert ['isFirstUpdate' in v[1] for v in result] == [True, True, True]
